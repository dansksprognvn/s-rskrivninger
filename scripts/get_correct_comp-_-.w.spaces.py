#!/usr/bin/python3

#------------------------------------------------------------------------------
# get_correct_comp-_-.w.spaces.py
# Script til at hente korrekte sammensætninger med flerords-førsteled. 
# Vel at mærke med det første ord case-insensitive. 
#------------------------------------------------------------------------------

import re
from subprocess import PIPE, Popen, STDOUT
import sys

def wordlist_portions(wl): 
  """
  :param wl: Liste med inputord
  :returns: Liste af lister med maks. 250 ord i hver. 
  """
  
  return [wl[x:x+250] for x in range(0, len(wl), 250)]  


def query_disjunction(portion): 
  """
  Lav en samlet søgning i CQP på ord som først=til=mølle-princippet og food=service-sektoren
  :param portion: Liste med inputord. (Portion der passer til CQP).
  :returns: Streng med |-disjunktion af inputordene. 
  """
  
  # Gør en liste klar til behandlede inputord
  prepped_list = []

  # Hvert inputord behandles
  for w in portion: 
    # Erstat første '=' med anførselstegn og case-insensitive-flag
    prepped_w = re.sub(r'^([^=]+)=(.+)$', r'"\1" %c "\2"', w)
    
    # Erstat resterende '=' med anførselstegn
    prepped_w = re.sub('=', r'" "', prepped_w)

    # Tilføj det færdigbehandlede ord til vores liste
    prepped_list.append(prepped_w)


  # Lav hele query-strengen
  tabulate = " ; tabulate query match .. matchend word, match sentence_niveau, match text_filnavn; "
  querystring = "; query =" + '|'.join(prepped_list) + tabulate

  return querystring


def run_cqp(querystring, corpus): 
  """
  Kør cqp på en portion af inputordene.
  :param querystring: Streng med søgeordene i en disjunktion
  :returns: Output fra cqp (format ..?)
  """

  # Første del af kommandopipe: echo kommandoen
  #cmd_1 = ["echo", """BERLINGSKE2011 ; query ="Marx" %c "Brothers-film"|"public" %c "service-kontrakten" ; tabulate query match .. matchend word;"""]
  cmd_1 = ["echo", corpus, querystring]
  echo = Popen(cmd_1, stdout = PIPE)

  # Anden del: Kør cqp
  cmd_2 = ["/usr/local/cwb-3.4.8/bin/cqp", "-c", "-r", "/corpora/registry"]
  cqp = Popen(cmd_2, stdin = echo.stdout, stdout = PIPE)

  # Tredje del: grep for at fjerne CQP-header
  cmd_3 = ["grep", "-v", "CQP version 3.4.8"]
  grep = Popen(cmd_3, stdin = cqp.stdout, stdout = PIPE)

  # Returner streng med outputtet
  return grep.stdout.read().decode('utf-8')


if __name__ == '__main__': 
  try:
    corpora = ['BERLINGSKE2011', 'BERLINGSKE2012', 'BERLINGSKE2013', 
'BERLINGSKE2014', 'BERLINGSKETIDENDE2004', 'BERLINGSKETIDENDE2005', 
'BERLINGSKETIDENDE2009', 'BERLINGSKETIDENDE2010', 'BERLINGSKETIDENDE2011', 
'EKSTRABLADET2004', 'EKSTRABLADET2005', 'INFORMATION2004', 'INFORMATION2005', 
'INFORMATION2009', 'INFORMATION2010', 'INFORMATION2011', 'INFORMATION2012', 
'INFORMATION2013', 'INFORMATION2014', 'JYLLANDSPOSTEN2004', 'JYLLANDSPOSTEN2005', 
'JYLLANDSPOSTEN2009', 'JYLLANDSPOSTEN2010', 'JYLLANDSPOSTEN2011', 'JYLLANDSPOSTEN2012', 
'JYLLANDSPOSTEN2013', 'JYLLANDSPOSTEN2014', 'KRISTELIGTDAGBLAD2009', 'KRISTELIGTDAGBLAD2010', 
'KRISTELIGTDAGBLAD2011', 'KRISTELIGTDAGBLAD2012', 'KRISTELIGTDAGBLAD2013', 
'KRISTELIGTDAGBLAD2014', 'POLITIKEN2005', 'POLITIKEN2009', 'POLITIKEN2010', 
'POLITIKEN2011', 'POLITIKEN2012', 'POLITIKEN2013', 'POLITIKEN2014', 
'WEEKENDAVISEN2004', 'WEEKENDAVISEN2005', 'WEEKENDAVISEN2009', 
'WEEKENDAVISEN2010', 'WEEKENDAVISEN2011', 'WEEKENDAVISEN2012', 
'WEEKENDAVISEN2013', 'WEEKENDAVISEN2014'] 
    if len(sys.argv) > 1:
      infile = sys.argv[1]
    else: 
      infile = '/home/philip/ortografisk_rangering/tekstanalyser/særskrivninger/udtraek/korrekte_former_m_flerordsførste_list.txt'
      sys.stderr.write('Ingen inputfil - bruger særskrivninger/udtraek/korrekte_former_m_flerordsførste_list.txt') 
    wordlist = open(infile).read().strip().split()
    list_of_wordlist_portions = wordlist_portions(wordlist)
    for portion in list_of_wordlist_portions: 
      querystring = query_disjunction(portion)
      for corpus in corpora: 
        query_result = run_cqp(querystring, corpus)
        sys.stdout.write(str(query_result))

  except IndexError as e: 
    print(e, '- husk at specificere inputfil med liste af sammensatte former med flerordsudtryk som førsteled')
